# Changelog Viewer

A changelog rendering helper package written in Spring boot

### Summary

It adds a controller for viewing change logs in browser by `/changelog` route.

(e.g. if your project URL is `http://localhost:8080`, then you can access changelog page on `http://localhost:8080/changelog`)

For default usage, CHANGELOG.md should be in default resource folder (`src/main/resources`)
or you should include file as a resource like shown in below pom.xml code. If
filename is different, then you should specify `fileName` in URL query. (e.g. `/changelog?fileName=Different.md`)

### Usage

You should add JitPack repository for importing dependency:

```
<repositories>
    <repository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </repository>
</repositories>
```

Then you can just add Maven dependency like this: 

 ```
 <dependencies>
     ..............
     <!-- changelog viewer !-->
     <dependency>
 	    <groupId>org.bitbucket.hprotravel</groupId>
 	    <artifactId>spring-changelog-viewer</artifactId>
     </dependency>
     ..............
 </dependencies>
  ...........
 <build>
    ...
    <resources>
        <resource>
           <!-- NOTE: This is required if resource usage is needed !-->
           <!-- because this is Spring Boot's default resource directory !-->
           <directory>src/main/resources</directory>
        </resource>
        <resource>
           <directory>${project.basedir}</directory>
              <includes>
                 <!-- This is example for file in root directory !-->
                 <!-- If file is in any subfolder, you should change it like subdir/CHANGELOG.md !-->
                 <include>CHANGELOG.md</include>
              </includes>
        </resource>
    </resources>
    ...
 </build>
 ```

After adding that, run `` mvn install`` to import dependency. And scan the package by 
adding or editing annotation in main application class.

If your application's running class is `MainApplication`, then annotiations will be like this:

```
@SpringBootApplication(scanBasePackages={"com.compass.changelog", ...})
............
public class MainApplication {
```

or

```
@SpringBootApplication
@ComponentScan(basePackages={"com.compass.changelog", ...})
............
public class MainApplication {
```