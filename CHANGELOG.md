## 0.3.0 (June 30, 2022)
  - Improvement - CK-103 - Remove github and maven executables and improve README
  - Improvement - CK-82 - Remove metglobal keyword from project
  - Update README.md (amend later)

## 0.2.0 (June 03, 2020)
  - Fetch file from resources path

## 0.1.0 (May 13, 2020)
  - Initial commit
  - Add license

